open Tea.App
open Tea.Html

(* Types *)
type number =
  | Zero
  | One
  | Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine

type operation =
  | Add
  | Subtract
  | Multiply
  | Divide

type symbol =
  | Digit of number
  | Arithmetic of operation
  | Dot

type msg =
  | Formulate of symbol
  | Calculate
  | Clear

type state = {
  expression: symbol list;
  result: float 
}

(* Update *)
(* Stringifying *)
let map_operation_to_string : operation -> string = fun operation ->
  match operation with
  | Add -> "+"
  | Subtract -> "-"
  | Multiply -> "x"
  | Divide -> "/"

let map_digit_to_string : number -> string = fun digit ->
  match digit with
  | Zero -> "0"
  | One -> "1"
  | Two -> "2"
  | Three -> "3"
  | Four -> "4"
  | Five -> "5"
  | Six -> "6"
  | Seven -> "7"
  | Eight -> "8"
  | Nine -> "9"

let map_symbol_to_string : symbol -> string = fun symbol ->
  match symbol with
  | Arithmetic operation -> map_operation_to_string operation
  | Digit number -> map_digit_to_string number
  | Dot -> "."

let stringify_expression : symbol list -> string = fun expression ->
  expression |> List.map map_symbol_to_string |> String.concat ""

(* Computation *)
let get_numbers : symbol list -> float list = fun expression ->
  List.fold_right
    (fun symbol acc ->
      match acc with
      | [] ->
        (match symbol with
        | Digit n -> [map_digit_to_string n]
        | _ -> [])
      | hd::tl ->
        (match symbol with
        | Arithmetic _ -> ""::acc
        | Digit n -> (hd ^ (map_digit_to_string n))::tl
        | Dot -> (hd ^ ".")::tl)
    )
    expression
    []
  |> List.map float_of_string

let get_operations : symbol list -> symbol list = fun expression ->
  List.append
    (List.filter
      (fun a ->
        match a with
        | Arithmetic _ -> true
        | _ -> false 
      )
      expression
    )
    [Arithmetic(Add)]

let select_addition_and_subtraction : symbol list -> symbol list = fun expression ->
  List.append
    (List.filter
      (fun symbol ->
        match symbol with
        | Arithmetic Add -> true 
        | Arithmetic Subtract -> true
        | _ -> false 
      )
      expression
    )
    [Arithmetic(Multiply)]

let exhaust_multiplication_and_division : symbol list -> float list = fun expression ->
  List.fold_right2
    (fun number operation acc ->
      match acc with
      | [] -> [number]
      | hd::tl ->
        (match operation with
        | Arithmetic Multiply -> (hd *. number)::tl
        | Arithmetic Divide -> (hd /. number)::tl
        | _ -> number::acc)
    )
    (get_numbers expression)
    (get_operations expression)
    []

let exhaust_addition_and_subtraction : symbol list -> float list -> float = fun expression multiplied ->
  List.fold_right2
    (fun number operation acc ->
      match operation with
      | Arithmetic Add -> (acc +. number)
      | Arithmetic Subtract -> (acc -. number)
      | _ -> number
    )
    multiplied
    (expression |> select_addition_and_subtraction)
    0.0

let compute_result : symbol list -> float = fun expression ->
  exhaust_addition_and_subtraction expression (exhaust_multiplication_and_division expression)

(* Update Expression *)
let will_zero_be_leading expression =
  match expression with
  | [] -> true
  | hd::_ ->
    match hd with
    | Arithmetic _ -> true
    | _ -> false

let rec has_dot_preceded expression =
  match expression with
  | [] -> false
  | [symbol] ->
    (match symbol with
    | Dot -> true
    | _ -> false)
  | hd::tl ->
    (match hd with
    | Dot -> true
    | Arithmetic _ -> false
    | Digit _ -> has_dot_preceded tl)

let remove_leading_operation expression =
  match expression with
  | [] -> []
  | hd::tl ->
    (match hd with
    | Arithmetic _ -> tl
    | _ -> expression)

let update : state -> msg -> state = fun model msg ->
  match msg with
  | Calculate -> {expression = model.expression; result = model.expression |> remove_leading_operation |> compute_result}
  | Clear -> {expression = []; result = 0.0}
  | Formulate symbol ->
      {expression =
        (match symbol with
        | Digit digit ->
          (match digit with
          | Zero ->
            (match (will_zero_be_leading model.expression) with
            | true -> model.expression
            | false -> symbol::model.expression)
          | _ -> symbol::model.expression)
        | Dot ->
            (match (has_dot_preceded model.expression) with
            | true -> model.expression
            | false -> symbol::model.expression)
        | Arithmetic _ ->
            (match model.expression with
            | [] -> []
            | [_] -> symbol::model.expression
            | hd::tl ->
                (match hd with
                | Digit _ -> symbol::model.expression
                | Arithmetic _ -> symbol::tl
                | Dot -> model.expression)))
      ; result = model.result}

(* View *)
let view model =
  div [ class' "calculator" ]
    [ div [ class' "expression" ]
      [ p [] [ text (List.rev model.expression |> stringify_expression) ]
      ; div [ class' "result" ]
        [ p [] [ text (string_of_float model.result)] ]
      ]
    ; button [ class' "ac"; onClick Clear ] [ text "AC"]
    ; button [ class' "divide"; onClick (Formulate(Arithmetic(Divide)))] [ text "/"]
    ; button [ class' "multiply"; onClick (Formulate(Arithmetic(Multiply)))] [ text "X"]
    ; button [ class' "subtract"; onClick (Formulate(Arithmetic(Subtract)))] [ text "-"]
    ; button [ class' "add"; onClick (Formulate(Arithmetic(Add)))] [ text "+"]
    ; button [ class' "equals"; onClick Calculate] [ text "="]
    ; button [ class' "dot"; onClick (Formulate(Dot))] [ text "."]
    ; button [ class' "zero"; onClick (Formulate(Digit(Zero)))] [ text "0"]
    ; button [ class' "one"; onClick (Formulate(Digit(One)))] [ text "1"]
    ; button [ class' "two"; onClick (Formulate(Digit(Two)))] [ text "2"]
    ; button [ class' "three"; onClick (Formulate(Digit(Three)))] [ text "3"]
    ; button [ class' "four"; onClick (Formulate(Digit(Four)))] [ text "4"]
    ; button [ class' "five"; onClick (Formulate(Digit(Five)))] [ text "5"]
    ; button [ class' "six"; onClick (Formulate(Digit(Six)))] [ text "6"]
    ; button [ class' "seven"; onClick (Formulate(Digit(Seven)))] [ text "7"]
    ; button [ class' "eight"; onClick (Formulate(Digit(Eight)))] [ text "8"]
    ; button [ class' "nine"; onClick (Formulate(Digit(Nine)))] [ text "9"]
    ]

let main =
  beginnerProgram {
    model = {expression = []; result = 0.0};
    update;
    view;
  }
